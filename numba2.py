from numba import cuda # Библиотека Nvidia для работы с GPU 
import numpy as np 

@cuda.jit('void(float32[:], float32[:], float32[:])') #Динамический компилятор Cuda 
def cuda_addition(a,b,c):
    """Поток будет выполнять эту функцию ядра."""
    i = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x # Отображение потока на индекс массива
    if i > c.size:
        return
    c[i] = a[i]+b[i] #Perform the addition
 
# Подробности об устройстве
device = cuda.get_current_device()

# Перенос с хоста на устройство
d_a = cuda.to_device(a)  # Перенос данных в глобальную память GPU
d_b = cuda.to_device(b)  # Перенос данных в глобальную память GPU
d_c = cuda.device_array_like(a)

tpb = device.WARP_SIZE       #blocksize или количество потоков на блок, стандартное значение = 32
bpg = int(np.ceil((n)/tpb))  # блоков на грид

cuda_addition[bpg, tpb](d_a, d_b, d_c) # вызов ядра

# Перенос вывода с устройства на хост
c = d_c.copy_to_host()
print(c)
